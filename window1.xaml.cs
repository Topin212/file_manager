﻿/*
 * Created by SharpDevelop.
 * User: Alex
 * Date: 14.03.2016
 * Time: 14:16
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace File_Manager
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{
		public static readonly DependencyProperty testDebugProperty = DependencyProperty.Register("testDebug", typeof(string), typeof(Window1));
		public static DependencyProperty loginDP = DependencyProperty.Register("loginProp", typeof(string), typeof(Window1));
		public static DependencyProperty passwordDP = DependencyProperty.Register("passwordProp", typeof(string), typeof(Window1));
		
		private bool loginSuccessful = false;
		
		public Window1()
		{
			InitializeComponent();
		}
		
		/// <summary>
		/// A property, used to get string from DP and set Dp as string.
		/// </summary>
		public string testDebug{
			get{
				return this.GetValue(testDebugProperty) as string;
			}
			set{
				this.SetValue(testDebugProperty, value);
			}
		}
		public string login{
			get{
				return this.GetValue(loginDP) as string;
			}
			set{
				this.SetValue(loginDP, value);
			}
		}
		public string pass{
			get{
				return this.GetValue(passwordDP) as string;
			}
			set{
				this.SetValue(passwordDP, value);
			}
		}
		
		/// <summary>
		/// Detects a click on the login button, and checks if there is a combination of login/password
		/// </summary>
		/// <param name="sender">The button itself?</param>
		/// <param name="e">routed event args</param>
		void onLoginButtonClick(object sender, RoutedEventArgs e)
		{
			passwordBox.GetValue(passwordDP);
			loginTextBox.GetValue(loginDP);
			//FIXME lame solution here
			login = loginTextBox.Text;
			pass = passwordBox.Password;
			//</lameSolution>
			
			if(login.Equals("topin212") && pass.Equals("zaqwsx"))
				loginSuccessful = true;
			else
				loginSuccessful = false;
			
			if(loginSuccessful)
			{
				mainWindow main = new mainWindow();
				main.Show();
				this.Close();
			}
			
		}
		
	}
}

