﻿/*
 * Created by SharpDevelop.
 * User: Alex
 * Date: 03/14/2016
 * Time: 15:36
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace File_Manager
{
	/// <summary>
	/// Interaction logic for mainWindow.xaml
	/// </summary>
	public partial class mainWindow : Window
	{
		/// <summary>
		/// Initializes the list
		/// </summary>
		private DependencyProperty cliDp = DependencyProperty.Register("cliDP", typeof(string), typeof(mainWindow));
		private readonly List<String> commandHistory = null;
		private String currentPath = null;
		private DirectoryInfo dirInfo = null;
		private DirectoryInfo[] dirs = null;
		
		
		Dictionary<String, String[]> tags = null;
		public string cliCommand{
			get{
				return this.GetValue(cliDp) as string;
			}
			set{
				this.SetValue(cliDp, value);
			}
		}
		
		public mainWindow()
		{
			commandHistory = new List<string>();
			//TODO initialize currentPath before actually creating dirinfo
			dirInfo = new DirectoryInfo(currentPath);
			InitializeComponent();
		}
		
		void cli_buttonDown(object sender, KeyEventArgs e)
		{
			if(!e.Key.Equals(Key.Enter))
				return;
			commandHistory.Add(cliTextBox.Text);
			
			ConsoleTextBlock.SetValue(cliDp, commandHistory[commandHistory.Count-1]);
			MessageBox.Show(commandHistory[commandHistory.Count-1], "You entered:");
			//TODO Finish CLI
		}
		void pathTextChanged(object sender, KeyEventArgs e)
		{
			if(!e.Key.Equals(Key.Enter))
				return;
			
			currentPath = pathTextBox.Text;
	        dirs = dirInfo.GetDirectories();
		}
	}
}

/*
    Maybe a dictionary will help? Where path is the key, and the value is a string array?

*/